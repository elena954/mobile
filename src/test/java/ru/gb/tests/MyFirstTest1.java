package ru.gb.tests;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class MyFirstTest1 {
    @Test
    public void CheckEmptyEmail() throws InterruptedException, MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "Pixel");
        capabilities.setCapability("platformVersion", "10");
        capabilities.setCapability("udid", "emulator-5554");
        capabilities.setCapability("automationName", "UiAutomator2");
        capabilities.setCapability("app", "C:/Users/len4i/Downloads/Android-NativeDemoApp-0.2.1.apk");

        MobileDriver driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

        Thread.sleep(2000);
        MobileElement loginMenuButton = (MobileElement) driver.findElementByAccessibilityId("Login");
        loginMenuButton.click();
        Thread.sleep(2000);
        MobileElement loginButton = (MobileElement) driver.findElementByXPath(
                "//android.view.ViewGroup[@content-desc=\"button-LOGIN\"]/android.view.ViewGroup");
        loginButton.click();
        Thread.sleep(2000);
        MobileElement errorText = (MobileElement) driver.findElementByXPath(
                "//android.widget.ScrollView[@content-desc=\"Login-screen\"]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView[1]");
        Assert.assertEquals(errorText.getText(),"Please enter a valid email address");

        Thread.sleep(2000);
        MobileElement signUp = (MobileElement) driver.findElementByXPath(
                "//android.view.ViewGroup[@content-desc=\"button-sign-up-container\"]/android.view.ViewGroup/android.widget.TextView");
        signUp.click();
        Thread.sleep(2000);
        MobileElement inputEmail = (MobileElement) driver.findElementByAccessibilityId("input-email");
        inputEmail.sendKeys("len4ik.kmx@gmail.com");
        Thread.sleep(2000);
        MobileElement inputPassword = (MobileElement) driver.findElementByAccessibilityId("input-password");
        inputPassword.sendKeys("3088");
        Thread.sleep(2000);
        MobileElement inputRepeatPassword = (MobileElement) driver.findElementByAccessibilityId(
                "input-repeat-password");
        inputRepeatPassword.sendKeys("3088");
        Thread.sleep(2000);
        MobileElement buttonSignUp = (MobileElement) driver.findElementByAccessibilityId("button-SIGN UP");
        buttonSignUp.click();
        Thread.sleep(2000);
        MobileElement popUpWindow = (MobileElement) driver.findElementById("android:id/button1");
        popUpWindow.click();
        Thread.sleep(2000);
        MobileElement errorPassword = (MobileElement) driver.findElementByXPath(
                "//android.widget.ScrollView[@content-desc=\"Login-screen\"]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView");
        Assert.assertEquals(errorPassword.getText(),"Please enter at least 8 characters");
        driver.quit();
    }
}
