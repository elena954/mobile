package ru.gb.homework3.pages;

import com.codeborne.selenide.Selenide;
import com.github.romankh3.image.comparison.ImageComparison;
import com.github.romankh3.image.comparison.ImageComparisonUtil;
import com.github.romankh3.image.comparison.model.ImageComparisonResult;
import com.github.romankh3.image.comparison.model.ImageComparisonState;
import io.qameta.allure.Step;
import ru.gb.homework3.locators.LoginPageLocators;

import java.awt.image.BufferedImage;
import java.io.File;

import static com.codeborne.selenide.Selenide.$;
import static org.testng.Assert.assertEquals;

public class LoginPage {
    private LoginPageLocators locator(){
        return new LoginPageLocators();
    }

    @Step("Вводим логин")
    public LoginPage inputEmail(){
        $(locator().inputEmail()).sendKeys("Len4ik.kmx@gmail.com");
        return this;
    }

    @Step("Вводим пароль")
    public LoginPage inputPassword(){
        $(locator().inputPassword()).sendKeys("01052022");
        return this;
    }

    @Step("Кликаем по кнопке 'логин' в форме")
    public PopUpWindow buttonLogin(){
        $(locator().buttonLogin()).click();
        return new PopUpWindow();
    }

    @Step("Делаем скриншот страницы login и сравниваем с требованием")
    public LoginPage checkingTheScreenshot(){
        BufferedImage expectedImage = ImageComparisonUtil
                .readImageFromResources("src/main/resources/expectedScreenshots/Login.png");

        File actualScreenshot = $(locator().loginScreen()).screenshot();

        BufferedImage actualImage = ImageComparisonUtil
                .readImageFromResources("screenshots/actual/" + actualScreenshot.getName());

        File resultDestination = new File("diff/diff_CheckLoginPageScreenshot.png");

        ImageComparisonResult imageComparisonResult = new ImageComparison(expectedImage, actualImage, resultDestination)
                .compareImages();
        assertEquals(ImageComparisonState.SIZE_MISMATCH,imageComparisonResult.getImageComparisonState());
        return this;
    }

}
