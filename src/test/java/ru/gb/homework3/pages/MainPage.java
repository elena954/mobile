package ru.gb.homework3.pages;

import io.qameta.allure.Step;
import ru.gb.homework3.locators.MainPageLocators;

import static com.codeborne.selenide.Selenide.$;

public class MainPage {
    private MainPageLocators locator(){
        return new MainPageLocators();
    }

    @Step("Кликаем по кнопке логина в меню и переходим на новую страницу логина")
    public LoginPage clickLoginMenuButton(){
        $(locator().loginButton()).click();
        return new LoginPage();
    }


}
