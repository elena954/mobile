package ru.gb.homework3.tests;


import io.qameta.allure.Description;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.gb.homework3.base.BaseTest;
import ru.gb.homework3.listeners.AllureListener;

@Listeners(AllureListener.class)
public class CheckingTheScreenshotLogin extends BaseTest {

    @Test
    @Description("Проверяем страницу login с помощью скриншота")
    public void LoginPageScreenshot(){
        openApp()
                .clickLoginMenuButton()
                .checkingTheScreenshot();
    }
}
