package ru.gb.homework3.locators;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class LoginPageLocators {

    public By inputEmail(){
        return MobileBy.AccessibilityId("input-email");
    }

    public By inputPassword(){
        return MobileBy.AccessibilityId("input-password");
    }

    public By buttonLogin(){
        return MobileBy.AccessibilityId("button-LOGIN");
    }

    public By loginScreen(){
        return MobileBy.AccessibilityId("Login-screen");
    }
}
