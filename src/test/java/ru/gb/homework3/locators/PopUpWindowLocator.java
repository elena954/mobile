package ru.gb.homework3.locators;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class PopUpWindowLocator {

//    хотела сделать по id, но он не хотел с ними работать пришлось xpath использовать
    public By successText(){
        return MobileBy
                .xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView");
    }

    public By clickOk(){
        return MobileBy
                .xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button");
    }
}
