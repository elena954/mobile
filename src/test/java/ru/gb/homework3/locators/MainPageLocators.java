package ru.gb.homework3.locators;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class MainPageLocators {

    public By loginButton(){
        return MobileBy.AccessibilityId("Login");
    }
}
