package ru.gb.homeWork7.locators;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class WebViewPageLocators {

    public By loading(){
        return MobileBy
                .xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView");
    }
}
