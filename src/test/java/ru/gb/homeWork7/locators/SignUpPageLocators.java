package ru.gb.homeWork7.locators;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class SignUpPageLocators {

    public By inputEmailSignUp(){
        return MobileBy.AccessibilityId("input-email");
    }

    public By inputPasswordSignUp(){
        return MobileBy.AccessibilityId("input-password");
    }

    public By inputConfirmPassword(){
        return MobileBy.AccessibilityId("input-repeat-password");
    }

    public By buttonSIGNUP(){
        return MobileBy.AccessibilityId("button-SIGN UP");
    }
}
