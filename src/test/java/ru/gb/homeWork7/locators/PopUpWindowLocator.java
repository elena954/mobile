package ru.gb.homeWork7.locators;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class PopUpWindowLocator {
    public By successText() {
        return MobileBy
                .xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView");
    }

    public By clickOk(){
        return MobileBy
                .xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button");
    }
}
