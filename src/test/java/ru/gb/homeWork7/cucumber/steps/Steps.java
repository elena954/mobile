package ru.gb.homeWork7.cucumber.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import ru.gb.homeWork7.base.BaseTest;
import ru.gb.homeWork7.pages.LoginPage;
import ru.gb.homeWork7.pages.WebViewPage;


public class Steps extends BaseTest {

    LoginPage loginPage;
    WebViewPage webViewPage;

    @Given("User in on Login page")
    public void user_in_on_Login_page() {
        loginPage = openApp().clickLoginMenuButton();
    }

    @When("The user enters the login")
    public void the_user_enters_the_login() {
        loginPage.inputEmail();
    }

    @When("The user enters the password")
    public void the_user_enters_the_password() {
        loginPage.inputPassword();
    }

    @Then("User click login button")
    public void user_click_login_button() {
        loginPage.buttonLogin();
    }

    @Then("We take a screenshot of the login page and compare it with the requirement")
    public void we_take_a_screenshot_of_the_login_page_and_compare_it_with_the_requirement() {
        loginPage.checkingTheScreenshot();
    }

    @Given("Button click web view")
    public void Button_click_web_view(){
        webViewPage = openApp().clickWebView();
    }

    @Then("Comparing the text {string}")
    public void comparing_the_text(String string){
        webViewPage.loading(string);
    }
}
