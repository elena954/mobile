package ru.gb.homeWork7.cucumber.hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;

public class Hooks {

    @Before
    public void prepareData(){
        System.out.println("Hello");
    }

    @After
    public void clearData(){
        System.out.println("Bye");
    }
}
