package ru.gb.homeWork7.tests;

import io.qameta.allure.Description;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.gb.homeWork7.base.BaseTest;
import ru.gb.homeWork7.listeners.AllureListener;

@Listeners(AllureListener.class)
public class SignUpTest extends BaseTest {

    public static final String MESSAGE = "You successfully signed up!";

    @Test
    @Description("Проверка регистрации и сообщения успеха")
    public void checkRegistrationAndMessageSuccess(){
        openApp()
                .clickLoginMenuButton()
                .buttonSignUp()
                .inputEmailSignUp()
                .inputPasswordSignUp()
                .inputConfirmPassword()
                .buttonSIGNUP()
                .signedUpText(MESSAGE)
                .signUpClickOk();
    }
}
