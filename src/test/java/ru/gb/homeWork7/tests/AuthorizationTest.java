package ru.gb.homeWork7.tests;

import io.qameta.allure.Description;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.gb.homeWork7.base.BaseTest;
import ru.gb.homeWork7.listeners.AllureListener;


@Listeners(AllureListener.class)
public class AuthorizationTest extends BaseTest {

    public static final String SUCCESS = "You are logged in!";

    @Test
    @Description("Проверка авторизации и сообщение успеха")
    public void checkingAuthorization(){
        openApp()
                .clickLoginMenuButton()
                .inputEmail()
                .inputPassword()
                .buttonLogin()
                .successText(SUCCESS)
                .clickOk();
    }
}
