package ru.gb.homeWork7.pages;

import io.qameta.allure.Step;
import ru.gb.homeWork7.locators.SignUpPageLocators;

import static com.codeborne.selenide.Selenide.$;

public class SignUpPage extends Throwable {
    private SignUpPageLocators locator(){
        return new SignUpPageLocators();
    }

    @Step("Ввод логина")
    public SignUpPage inputEmailSignUp(){
        $(locator().inputEmailSignUp()).sendKeys("Len4ik.kmx@gmail.com");
        return this;
    }

    @Step("Ввод пароля")
    public SignUpPage inputPasswordSignUp(){
        $(locator().inputPasswordSignUp()).sendKeys("01052022");
        return this;
    }

    @Step("Повторный ввод пароля")
    public SignUpPage inputConfirmPassword(){
        $(locator().inputConfirmPassword()).sendKeys("01052022");
        return this;
    }

    @Step("Клик кнопка зарегистрироваться")
    public PopUpSignUpPage buttonSIGNUP(){
        $(locator().buttonSIGNUP()).click();
        return new PopUpSignUpPage();
    }
}
