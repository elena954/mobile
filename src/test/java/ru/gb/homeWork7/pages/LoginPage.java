package ru.gb.homeWork7.pages;

import com.github.romankh3.image.comparison.ImageComparison;
import com.github.romankh3.image.comparison.ImageComparisonUtil;
import com.github.romankh3.image.comparison.model.ImageComparisonResult;
import com.github.romankh3.image.comparison.model.ImageComparisonState;
import io.qameta.allure.Step;
import ru.gb.homeWork7.locators.LoginPageLocators;


import java.awt.image.BufferedImage;
import java.io.File;

import static com.codeborne.selenide.Selenide.$;
import static org.testng.Assert.assertEquals;

public class LoginPage {
    private LoginPageLocators locator(){
        return new LoginPageLocators();
    }

    @Step("Ввод логина")
    public LoginPage inputEmail(){
        $(locator().inputEmail()).sendKeys("Len4ik.kmx@gmail.com");
        return this;
    }

    @Step("Ввод пароля")
    public LoginPage inputPassword(){
        $(locator().inputPassword()).sendKeys("01052022");
        return this;
    }

    @Step("Клик по кнопке 'логин' в форме")
    public PopUpWindow buttonLogin(){
        $(locator().buttonLogin()).click();
        return new PopUpWindow();
    }

    @Step("Делаем скриншот страницы login и сравниваем с требованием")
    public LoginPage checkingTheScreenshot(){
        BufferedImage expectedImage = ImageComparisonUtil
                .readImageFromResources("src/main/resources/expectedScreenshots/Login.png");

        File actualScreenshot = $(locator().loginScreen()).screenshot();

        BufferedImage actualImage = ImageComparisonUtil
                .readImageFromResources("screenshots/actual/" + actualScreenshot.getName());

        File resultDestination = new File("diff/diff_CheckLoginPageScreenshot.png");

        ImageComparisonResult imageComparisonResult = new ImageComparison(expectedImage, actualImage, resultDestination)
                .compareImages();
        assertEquals(ImageComparisonState.SIZE_MISMATCH,imageComparisonResult.getImageComparisonState());
        return this;
    }

    @Step("Клик по кнопке зарегистрироваться")
    public SignUpPage buttonSignUp(){
        $(locator().buttonSignUp()).click();
        return new SignUpPage();
    }
}
