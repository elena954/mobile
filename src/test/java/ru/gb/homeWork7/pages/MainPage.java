package ru.gb.homeWork7.pages;

import io.qameta.allure.Step;
import ru.gb.homeWork7.locators.MainPageLocators;


import static com.codeborne.selenide.Selenide.$;

public class MainPage {
    private MainPageLocators locator(){
        return new MainPageLocators();
    }

    @Step("Кликаем по кнопке логина в меню и переходим на новую страницу логина")
    public LoginPage clickLoginMenuButton(){
        $(locator().loginButton()).click();
        return new LoginPage();
    }

    @Step("Кликаем по кнопке WwbView в меню и переходим на новую страницу")
    public WebViewPage clickWebView(){
        $(locator().clickWebView()).click();
        return new WebViewPage();
    }
}
