package ru.gb.homeWork7.pages;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import ru.gb.homeWork7.locators.WebViewPageLocators;


import static com.codeborne.selenide.Selenide.$;

public class WebViewPage {
    private WebViewPageLocators locator() {
        return new WebViewPageLocators();
    }

    @Step("Проверяем текст загрузки")
    public WebViewPage loading(String text) {
        $(locator().loading()).shouldHave(Condition.text(text));
        return this;
    }
}
