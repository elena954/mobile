package ru.gb.homeWork7.pages;

import com.codeborne.selenide.Condition;
import ru.gb.homeWork7.locators.PopUpSignUpPageLocators;

import static com.codeborne.selenide.Selenide.$;

public class PopUpSignUpPage {
    private PopUpSignUpPageLocators locator(){
        return new PopUpSignUpPageLocators();
    }


    public PopUpSignUpPage signedUpText(String text) {
        $(locator().signedUpText()).shouldHave(Condition.text(text));
        return this;
    }

    public SignUpPage signUpClickOk(){
        $(locator().signUpClickOk()).click();
        return new SignUpPage();
    }
}
