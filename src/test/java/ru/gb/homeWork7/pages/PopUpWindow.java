package ru.gb.homeWork7.pages;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import ru.gb.homeWork7.locators.PopUpWindowLocator;


import static com.codeborne.selenide.Selenide.$;

public class PopUpWindow {

    private PopUpWindowLocator locator(){
        return new PopUpWindowLocator();
    }

    @Step("Проверяем текст успеха")
    public PopUpWindow successText(String text) {
        $(locator().successText()).shouldHave(Condition.text(text));
        return new PopUpWindow();
    }

    @Step("Кликаем Ок")
    public LoginPage clickOk(){
        $(locator().clickOk()).click();
        return new LoginPage();
    }
}
