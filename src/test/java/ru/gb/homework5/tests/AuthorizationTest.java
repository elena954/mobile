package ru.gb.homework5.tests;



import io.qameta.allure.Description;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.gb.homework5.base.BaseTest;
import ru.gb.homework5.listeners.AllureListener;


@Listeners(AllureListener.class)
public class AuthorizationTest extends BaseTest {

    public static final String SUCCESS = "You are logged in!";

    @Test
    @Description("Проверяем авторизацию и сообщение с успехом")
    public void checkingAuthorization(){
        openApp()
                .clickLoginMenuButton()
                .inputEmail()
                .inputPassword()
                .buttonLogin()
                .successText(SUCCESS)
                .clickOk();
    }
}
