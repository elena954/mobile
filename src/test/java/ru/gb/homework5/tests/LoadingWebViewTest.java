package ru.gb.homework5.tests;

import io.qameta.allure.Description;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.gb.homework5.base.BaseTest;
import ru.gb.homework5.listeners.AllureListener;

@Listeners(AllureListener.class)
public class LoadingWebViewTest extends BaseTest {

    public static final String LOADING = "LOADING...";

    @Test
    @Description("Проверяем текст загрузки")
    public void checkingClicks(){
        openApp()
                .clickWebView()
                .loading(LOADING);
    }
}
