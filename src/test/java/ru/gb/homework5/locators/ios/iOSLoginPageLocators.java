package ru.gb.homework5.locators.ios;

import org.openqa.selenium.By;
import ru.gb.homework5.locators.interfaceLocators.LoginPageLocators;

public class iOSLoginPageLocators implements LoginPageLocators {
    @Override
    public By inputEmail() {
        return null;
    }

    @Override
    public By inputPassword() {
        return null;
    }

    @Override
    public By buttonLogin() {
        return null;
    }

    @Override
    public By loginScreen() {
        return null;
    }
}
