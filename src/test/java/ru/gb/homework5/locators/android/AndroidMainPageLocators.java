package ru.gb.homework5.locators.android;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;
import ru.gb.homework5.locators.interfaceLocators.MainPageLocators;

public class AndroidMainPageLocators implements MainPageLocators {

    @Override
    public By clickWebView() {
        return MobileBy.AccessibilityId("WebView");
    }

    @Override
    public By loginButton() {
        return MobileBy.AccessibilityId("Login");
    }
}
