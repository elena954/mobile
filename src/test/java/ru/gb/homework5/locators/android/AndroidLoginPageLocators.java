package ru.gb.homework5.locators.android;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;
import ru.gb.homework5.locators.interfaceLocators.LoginPageLocators;

public class AndroidLoginPageLocators implements LoginPageLocators {
    @Override
    public By inputEmail() {
        return MobileBy.AccessibilityId("input-email");
    }

    @Override
    public By inputPassword() {
        return MobileBy.AccessibilityId("input-password");
    }

    @Override
    public By buttonLogin() {
        return MobileBy.AccessibilityId("button-LOGIN");
    }

    @Override
    public By loginScreen() {
        return MobileBy.AccessibilityId("Login-screen");
    }
}
