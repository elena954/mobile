package ru.gb.homework5.locators.android;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;
import ru.gb.homework5.locators.interfaceLocators.WebViewPageLocators;

public class AndroidWebViewPageLocators implements WebViewPageLocators {

    @Override
    public By loading() {
        return MobileBy.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView");
    }
}
