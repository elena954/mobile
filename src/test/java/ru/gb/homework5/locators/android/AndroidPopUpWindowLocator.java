package ru.gb.homework5.locators.android;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;
import ru.gb.homework5.locators.interfaceLocators.PopUpWindowLocator;

public class AndroidPopUpWindowLocator implements PopUpWindowLocator {
    @Override
    public By successText() {
        return MobileBy
                .xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget" +
                        ".FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget" +
                        ".ScrollView/android.widget.LinearLayout/android.widget.TextView");
    }

    @Override
    public By clickOk() {
        return MobileBy
                .xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget" +
                        ".FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget" +
                        ".LinearLayout/android.widget.Button");
    }
}
