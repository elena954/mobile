package ru.gb.homework5.locators;

import ru.gb.homework5.locators.android.AndroidLoginPageLocators;
import ru.gb.homework5.locators.android.AndroidMainPageLocators;
import ru.gb.homework5.locators.android.AndroidPopUpWindowLocator;
import ru.gb.homework5.locators.android.AndroidWebViewPageLocators;
import ru.gb.homework5.locators.interfaceLocators.LoginPageLocators;
import ru.gb.homework5.locators.interfaceLocators.MainPageLocators;
import ru.gb.homework5.locators.interfaceLocators.PopUpWindowLocator;
import ru.gb.homework5.locators.interfaceLocators.WebViewPageLocators;
import ru.gb.homework5.locators.ios.iOSLoginPageLocators;
import ru.gb.homework5.locators.ios.iOSMainPageLocators;
import ru.gb.homework5.locators.ios.iOSPopUpWindowLocator;
import ru.gb.homework5.locators.ios.iOSWebViewPageLocators;

public class LocatorService {

    public static final MainPageLocators MAIN_PAGE_LOCATORS = System.getProperty("platform")
            .equals("Android")? new AndroidMainPageLocators() : new iOSMainPageLocators();

    public static final LoginPageLocators LOGIN_PAGE_LOCATORS = System.getProperty("platform")
            .equals("Android")? new AndroidLoginPageLocators() : new iOSLoginPageLocators();

    public static final PopUpWindowLocator POP_UP_WINDOW_LOCATOR = System.getProperty("platform")
            .equals("Android")? new AndroidPopUpWindowLocator() : new iOSPopUpWindowLocator();

    public static final WebViewPageLocators WEB_VIEW_PAGE_LOCATORS = System.getProperty("platform")
            .equals("Android")? new AndroidWebViewPageLocators() : new iOSWebViewPageLocators();
}
