package ru.gb.homework5.locators.interfaceLocators;

import org.openqa.selenium.By;

public interface PopUpWindowLocator {
    By successText();
    By clickOk();
}
