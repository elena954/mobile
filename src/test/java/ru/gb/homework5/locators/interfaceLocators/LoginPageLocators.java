package ru.gb.homework5.locators.interfaceLocators;

import org.openqa.selenium.By;

public interface LoginPageLocators {
    By inputEmail();
    By inputPassword();
    By buttonLogin();
    By loginScreen();
}
