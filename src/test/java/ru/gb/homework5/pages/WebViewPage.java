package ru.gb.homework5.pages;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import ru.gb.homework5.locators.LocatorService;
import ru.gb.homework5.locators.interfaceLocators.WebViewPageLocators;

import static com.codeborne.selenide.Selenide.$;

public class WebViewPage {

    private WebViewPageLocators locator() {
        return LocatorService.WEB_VIEW_PAGE_LOCATORS;
    }

    @Step("Проверяем текст загрузки")
    public WebViewPage loading(String text) {
        $(locator().loading()).shouldHave(Condition.text(text));
        return this;
    }
}
