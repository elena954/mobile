package ru.gb.homework5.pages;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import ru.gb.homework3.pages.LoginPage;
import ru.gb.homework5.locators.LocatorService;
import ru.gb.homework5.locators.interfaceLocators.PopUpWindowLocator;

import static com.codeborne.selenide.Selenide.$;

public class PopUpWindow {

    private PopUpWindowLocator locator(){
        return LocatorService.POP_UP_WINDOW_LOCATOR;
    }

    @Step("Проверяем текст успеха")
    public PopUpWindow successText(String text) {
        $(locator().successText()).shouldHave(Condition.text(text));
        return this;
    }

    @Step("Кликаем Ок")
    public ru.gb.homework3.pages.LoginPage clickOk(){
        $(locator().clickOk()).click();
        return new LoginPage();
    }
}
