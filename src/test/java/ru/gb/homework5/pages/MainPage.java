package ru.gb.homework5.pages;

import io.qameta.allure.Step;
import ru.gb.homework5.locators.LocatorService;
import ru.gb.homework5.locators.interfaceLocators.MainPageLocators;

import static com.codeborne.selenide.Selenide.$;

public class MainPage {
    private MainPageLocators locator(){
        return LocatorService.MAIN_PAGE_LOCATORS;
    }

    @Step("Кликаем по кнопке логина в меню и переходим на новую страницу логина")
    public LoginPage clickLoginMenuButton(){
        $(locator().loginButton()).click();
        return new LoginPage();
    }

    @Step("Кликаем по кнопке WwbView в меню и переходим на новую страницу")
    public WebViewPage clickWebView(){
        $(locator().clickWebView()).click();
        return new WebViewPage();
    }
}
