package delete;

import java.io.File;

public class DeleteFolderAllure {

    public static void main (String[] args){
        String folder = "C:\\Users\\len4i\\IdeaProjects\\mobile\\allure-results";
        recursiveDelete(new File(folder));
    }

    private static void recursiveDelete(File file) {
        if(!file.exists())
            return;

        if (file.isDirectory()){
            for (File f :file.listFiles()){
                recursiveDelete(f);
            }
        }
        file.delete();
        System.out.println("Delete" + file.getAbsolutePath());
    }
}
